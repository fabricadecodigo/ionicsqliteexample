import { Injectable } from '@angular/core';
import { DatabaseProvider } from '../database/database';

@Injectable()
export class ProdutoProvider {

  constructor(public db: DatabaseProvider) { }

  private insert(produto: any) {
    let sql = 'insert into produtos (nome, descricao, categoria, preco, dataCadastro, tamanho, ativo) values (?, ?, ?, ?, ?, ?, ?)';
    let data = [produto.nome, produto.descricao, produto.categoria, produto.preco, produto.dataCadastro, produto.tamanho, (produto.ativo ? 1 : 0)];

    return this.db.executeSQL(sql, data).catch((e) => console.error(e));
  }

  private update(produto: any) {
    let sql = 'update produtos set nome = ?, descricao = ?, categoria = ?, preco = ?, dataCadastro = ?, tamanho = ?, ativo = ? where id = ?';
    let data = [produto.nome, produto.descricao, produto.categoria, produto.preco, produto.dataCadastro, produto.tamanho, (produto.ativo ? 1 : 0), produto.id];

    return this.db.executeSQL(sql, data).catch((e) => console.error(e));
  }

  save(produto: any) {
    if (produto.id) {
      return this.update(produto);
    } else {
      return this.insert(produto);
    }
  }

  /**
   * Pupula uma array de produtos com as informações do banco de dados
   * @param rows registros de produtos do banco de dados
   */
  private popularProdutos(rows: any) {
    let produtos = [];

    if (rows.length > 0) {
      for (var i = 0; i < rows.length; i++) {
        let produto = rows.item(i);
        produtos.push({
          id: produto.id,
          nome: produto.nome,
          descricao: produto.descricao,
          categoria: this.getCategoriaDescricao(produto.categoria),
          tamanho: this.getTamanhoDescricao(produto.tamanho),
          preco: produto.preco,
          ativo: produto.ativo == 1 ? true : false
        });
      }
    }

    return produtos;
  }

  /**
   * Busca alguns dados de todos os produtos ativos
   */
  getAllAtivos() {
    let sql = 'select id, nome, descricao, categoria, tamanho, preco, ativo from produtos where ativo = ?';

    return new Promise((resolve) => {
      this.db.executeSQL(sql, [1]) // 1 para produtos ativos
        .then((data: any) => {
          let produtos = this.popularProdutos(data.rows);
          resolve(produtos);
        });
    });
  }

  /**
   * Busca alguns dados de todos os produtos
   */
  getAll() {
    return new Promise((resolve) => {
      let sql = 'select id, nome, descricao, categoria, tamanho, preco, ativo from produtos';

      this.db.executeSQL(sql)
        .then((data: any) => {
          let produtos = this.popularProdutos(data.rows);
          resolve(produtos);
        });
    });
  }

  /**
   * Busca um produto no banco de dados
   * @param id identificador do produto
   */
  get(id: number) {
    return new Promise((resolve) => {
      let sql = 'select * from produtos where id = ?';

      this.db.executeSQL(sql, [id])
        .then((data: any) => {
          if (data.rows.length > 0) {
            let produto = data.rows.item(0);
            resolve(produto);
          }
          resolve(null);
        });
    });
  }

  remove(id: number) {
    let sql = 'delete from produtos where id = ?';
    return this.db.executeSQL(sql, [id]);
  }

  private getCategoriaDescricao(valor: number) {
    if (valor == 1) {
      return 'Hambúrguer';
    } else if (valor == 2) {
      return 'Refrigerante';
    } else if (valor == 3) {
      return 'Batata';
    }
  }

  private getTamanhoDescricao(valor: string) {
    if (valor == 'p') {
      return 'Pequeno';
    } else if (valor == 'm') {
      return 'Médio';
    } else if (valor == 'g') {
      return 'Grande';
    }
  }
}
