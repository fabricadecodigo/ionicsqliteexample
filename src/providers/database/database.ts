import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { Injectable } from '@angular/core';

@Injectable()
export class DatabaseProvider {

  constructor(public sqlite: SQLite) { }

  private getDB() {
    return this.sqlite.create({
      name: 'produtos.db',
      location: 'default'
    });
  }

  /**
   * Cria as tabelas no banco de dados
   * @param db banco de dados sqlite
   */
  private createTables(db: SQLiteObject) {
    db.sqlBatch([
      'CREATE TABLE IF NOT EXISTS produtos (id INTEGER primary key AUTOINCREMENT NOT NULL, nome TEXT, descricao TEXT, categoria INTEGER, preco REAL, dataCadastro DATE, tamanho TEXT, ativo INTEGER)'
    ])
      .then(() => console.log('Tabelas criadas'))
      .catch(e => console.error('Erro ao criar as tabelas', e));
  }

  /**
   * Cria o banco de dados
   */
  createDatabase() {
    return this.getDB()
      .then((db: SQLiteObject) => {
        // Criando as tabelas
        this.createTables(db);
      })
      .catch(e => console.log(e));
  }

  executeSQL(sql: string, params?: any[]) {
    return new Promise((resolve, reject) => {
      this.getDB()
        .then((db: SQLiteObject) => {
          db.executeSql(sql, params)
            .then((result: any) => {
              resolve(result);
            })
            .catch((e) => {
              reject(e);
            });
        });
    });
  }
}
