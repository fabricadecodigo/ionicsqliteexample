import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ProdutoProvider } from '../../providers/produto/produto';

@IonicPage()
@Component({
  selector: 'page-edit-produto',
  templateUrl: 'edit-produto.html',
})
export class EditProdutoPage {
  form: FormGroup;

  constructor(public navCtrl: NavController, public navParams: NavParams, private formBuilder: FormBuilder, private toast: ToastController, private produtoProvider: ProdutoProvider ) {
    // dados padrão do cadastro
    let produto = {
      categoria: '1',
      tamanho: 'p',
      ativo: true
    }

    // o formulário sempre é iniciado com os dados padrão para o objeto this.form não ser nulo na tela e com isso dar erro
    this.createForm(produto);

    if (navParams.data.id) {
      this.produtoProvider.get(navParams.data.id)
        .then((produto: any) => {
          if (produto) {
            // atualizo os valores do formulario
            this.form.patchValue(produto)
          } else {
            this.toast.create({ message: `Produto de id ${navParams.data.id} não encontrado.`, duration: 2000 }).present();
          }
        })
        .catch(() => {
          this.toast.create({ message: 'Erro ao buscar as informações do produto', duration: 2000 }).present();
        });
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EditProdutoPage');
  }

  private createForm(produto: any) {
    this.form = this.formBuilder.group({
      id: [produto.id],
      nome: [produto.nome, [Validators.required, Validators.minLength(2)]],
      descricao: [produto.descricao, [Validators.required, Validators.minLength(2)]],
      categoria: [produto.categoria],
      preco: [produto.preco, [Validators.required]],
      dataCadastro: [produto.dataCadastro, [Validators.required]],
      tamanho: [produto.tamanho],
      ativo: [produto.ativo]
    })
  }

  salvar() {
    if (this.form.valid) {
      this.produtoProvider.save(this.form.value)
        .then((result: any) => {
          console.log(result);
          this.toast.create({ message: 'Produto salvo com sucesso', duration: 1000 }).present();
        })
    }
  }
}
