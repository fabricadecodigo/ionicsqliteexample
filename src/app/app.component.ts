import { Component } from '@angular/core';
import { Platform, ToastController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { DatabaseProvider } from '../providers/database/database';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage: any;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, toast: ToastController, db: DatabaseProvider) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();

      db.createDatabase()
        .then(() => {
          // se criar o banco de dados com sucesso eu exibo a home page.
          // esse tratamento é para não tentar acessar uma tabela sem que ela exista.
          this.openHomePage(splashScreen);
        })
        .catch(() => {
          toast.create({ message: 'Erro ao criar o banco de dados.' }).present();
        });
    });
  }

  openHomePage(splashScreen: SplashScreen) {
    splashScreen.hide();
    this.rootPage = 'HomePage';
  }
}

