import { Component } from '@angular/core';
import { IonicPage, NavController, ToastController, AlertController } from 'ionic-angular';
import { ProdutoProvider } from '../../providers/produto/produto';

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {
  produtos = [];

  constructor(public navCtrl: NavController, public toast: ToastController, public produtoProvider: ProdutoProvider, public alert: AlertController) {
  }

  /**
   * Estou usando o ionViewDidEnter para toda vez que for entrar na tela carregar a lista de novo
   * Em alguns casos isso não pode ser muito legal para a performance do app
   * Logo CUIDADO! =]
   */
  ionViewDidEnter() {
    this.exibirProdutos(this.produtoProvider.getAllAtivos());
  }

  exibirProdutos(promise: Promise<{}>) {
    promise
      .then((produtos: any[]) => {
        this.produtos = produtos;
      })
      .catch(() => {
        this.toast.create({ message: 'Erro ao buscar os produtos', duration: 1000 }).present();
      });
  }

  addProduto() {
    this.navCtrl.push('EditProdutoPage');
  }

  editProduto(id: number) {
    this.navCtrl.push('EditProdutoPage', { id: id });
  }

  buscarProdutos(evt: any) {
    if (evt.value) {
      this.exibirProdutos(this.produtoProvider.getAll())
    } else {
      this.exibirProdutos(this.produtoProvider.getAllAtivos())
    }
  }

  remover(produto: any) {
    this.alert.create({
      title: 'Excluir produto : ' + produto.nome,
      subTitle: 'Essa operação não poderá ser desfeita.',
      message: 'Confirma a exclusão do produto?',
      buttons: [
        {
          text: 'Remover',
          handler: () => {
            this.removerProduto(produto);
          }
        },
        {
          text: 'Cancelar'
        }
      ]
    }).present();
  }

  removerProduto(produto: any) {
    this.produtoProvider.remove(produto.id)
      .then(() => {
        // Removendo do array de items
        var index = this.produtos.indexOf(produto);
        this.produtos.splice(index, 1);
        this.toast.create({ message: 'Produto removido.', duration: 2000 }).present();
      });
  }

}
